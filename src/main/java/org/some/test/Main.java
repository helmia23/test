package org.some.test;

import perf.Command;
import perf.freq.Frequencer;
import perf.json.Config;
import perf.mq.Injector;

public class Main {

    public static void main(String[] args)  {

        Config config = Config.getConfigFromFile();
        final Injector injector = new Injector(config);
        Command command = new Command() {
            @Override
            public void execute(String message) {
                injector.send(message);
            }
        };
        Frequencer frequencer = new Frequencer(config);
        frequencer.runCommandInGaussianFrequency(command, new String[]{"a", "b", "c"});
    }


}
